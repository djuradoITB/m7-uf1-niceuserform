package com.example.niceuserform;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.MaterialAutoCompleteTextView;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class RegisterScreen extends AppCompatActivity {

    TextInputLayout username;
    TextInputLayout password;
    TextInputLayout repeatPassword;
    TextInputLayout email;
    MaterialButton register;
    MaterialButton login;
    MaterialCheckBox tac;

    AutoCompleteTextView gender;
    ArrayList<String> alGender;
    ArrayAdapter<String> arrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        username = findViewById(R.id.editTextRegisterUsername);
        password = findViewById(R.id.editTextRegisterPassword);
        repeatPassword = findViewById(R.id.editTextRegisterRepeatPassword);
        email = findViewById(R.id.editTextRegisterEmail);
        register = findViewById(R.id.buttonRegisterRegister);
        login = findViewById(R.id.buttonRegisterLogin);
        gender = findViewById(R.id.textViewRegisterGenderPronoun);
        tac = findViewById(R.id.checkBoxTaC);


        alGender = new ArrayList<>();
        alGender.add("they/them");
        alGender.add("she/her");
        alGender.add("he/him");
        alGender.add("other/prefer not to say");

        arrayAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.gender_item_list, alGender);
        gender.setAdapter(arrayAdapter);
        gender.setThreshold(1);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean b1, b2, b3, b4, b5;
                String text = username.getEditText().getText().toString();
                if (text.isEmpty()) {
                    username.setError(getString(R.string.errorTextRequired));
                    b1 = false;
                } else {
                    username.setError(null);
                    b1 = true;
                }

                text = password.getEditText().getText().toString();
                if (text.isEmpty()) {
                    password.setError(getString(R.string.errorTextRequired));
                    b2 = false;
                }else {
                    password.setError(null);
                    b2 = true;
                }

                text = repeatPassword.getEditText().getText().toString();
                if (text.isEmpty()) {
                    repeatPassword.setError(getString(R.string.errorTextRequired));
                    b3 = false;
                } else {
                    repeatPassword.setError(null);
                    b3 = true;
                }

                text = email.getEditText().getText().toString();
                if (text.isEmpty()) {
                    email.setError(getString(R.string.errorTextRequired));
                    b4 = false;
                } else {
                    email.setError(null);
                    b4 = true;
                }

                if (!tac.isChecked()) {
                    tac.setError(getString(R.string.errorTextRequired));
                    b5 = false;
                } else {
                    tac.setError(null);
                    b5 = true;
                }

                if (b1 && b2 && b3 && b4 && b5) {
                    Intent i = new Intent(RegisterScreen.this, WelcomeScreen.class);
                    startActivity(i);
                }

            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegisterScreen.this, LoginScreen.class);
                startActivity(i);
            }
        });

    }
}